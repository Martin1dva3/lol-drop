from django.urls import include,path
from rest_framework import routers

import reaction_api
from . import views
router = reaction_api.urls.router


urlpatterns = [
   path('api/', include(router.urls)),
   
]