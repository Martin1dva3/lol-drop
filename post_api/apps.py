from django.apps import AppConfig


class PostApiConfig(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'post_api'
