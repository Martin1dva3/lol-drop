from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Post(models.Model):
    date = models.DateTimeField(default=datetime.now)
    media = models.URLField(max_length=600)
    views = models.IntegerField(default=0)
    template = models.BooleanField(default=False)
    owner = models.CharField(max_length=500,default="")
    ownerid = models.ForeignKey('auth.User', related_name='post', on_delete=models.CASCADE,default=0,blank=True, null=True)

    class Meta:
        ordering = ('-date',)
   
# Create your models here.
