
from contextlib import nullcontext
import json

from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from post_api.serializer import PostSerializer
from post_api.models import Post
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action

class PostViewSet(viewsets.ModelViewSet):
   queryset = Post.objects.all()
   serializer_class = PostSerializer

   def create(self, request, *args, **kwargs):
      cre = super(PostViewSet, self).create(request)
      return Response({'created': True},status=status.HTTP_201_CREATED)
   
   
   def destroy(self, request, *args, **kwargs):
       post = self.get_object()
       post.delete()
       return Response({'deleted':True},status=status.HTTP_204_NO_CONTENT)
   
   

# Create your views here.
