# Generated by Django 3.2.13 on 2022-05-10 11:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post_api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='template',
            field=models.BooleanField(default=False),
        ),
    ]
