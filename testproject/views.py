from allauth.account.views import ConfirmEmailView
from django.contrib.auth import get_user_model

class CustomConfirmEmailView(ConfirmEmailView):    
    def get(self, *args, **kwargs):        
        try:            
            self.object = self.get_object()        
        except Http404:            
            self.object = None        
            user = get_user_model().objects.get(email=self.object.email_address.email)        
            redirect_url = reverse('user', args=(user.id,))
        return redirect(redirect_url)