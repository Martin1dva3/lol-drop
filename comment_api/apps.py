from django.apps import AppConfig


class CommentApiConfig(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'comment_api'
