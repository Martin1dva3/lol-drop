from datetime import datetime
from django.db import models
from post_api.models import Post

 
class Comment(models.Model):
    date = models.DateTimeField(default=datetime.now)
    text = models.CharField(max_length=600, default="")
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE)
    owner = models.ForeignKey('auth.User', related_name='comment', on_delete=models.CASCADE,default=0)
    
# Create your models here.
