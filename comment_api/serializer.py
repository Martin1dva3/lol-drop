from tokenize import Comment
from rest_framework import serializers
from comment_api.models import Comment
from django.contrib.auth.hashers import make_password


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id','date', 'text','owner','post')

    