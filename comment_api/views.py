from django.shortcuts import render
from rest_framework import viewsets
from comment_api.serializer import CommentSerializer
from comment_api.models import Comment

class CommentViewSet(viewsets.ModelViewSet):
   queryset = Comment.objects.all()
   serializer_class = CommentSerializer


# Create your views here.
