from django.urls import include,path
from rest_framework import routers
from post_api import views as postviews
from comment_api import views as commentviews
from reaction_api import views as reactionviews


from . import views
router = routers.DefaultRouter()
router.register(r'posts', postviews.PostViewSet)
router.register(r'comments', commentviews.CommentViewSet)
router.register(r'reactions', reactionviews.ReactionViewSet)


urlpatterns = [
   path('', include(router.urls)),
   
]