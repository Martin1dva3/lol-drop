from django.shortcuts import render
from rest_framework import viewsets
from reaction_api.serializer import ReactionSerializer
from reaction_api.models import Reaction

class ReactionViewSet(viewsets.ModelViewSet):
   queryset = Reaction.objects.all()
   serializer_class = ReactionSerializer


# Create your views here.
