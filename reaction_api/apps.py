from django.apps import AppConfig


class ReactionApiConfig(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'reaction_api'
