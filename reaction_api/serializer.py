
from rest_framework import serializers
from reaction_api.models import Reaction
from django.contrib.auth.hashers import make_password


class ReactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reaction
        fields = ('id','date', 'reaction','owner','postid')

    