from datetime import datetime
from django.db import models
from post_api.models import Post
 
class Reaction(models.Model):
    date = models.DateTimeField(default=datetime.now)
    reaction = models.IntegerField()
    postid = models.ForeignKey(to=Post, on_delete=models.CASCADE)
    owner = models.ForeignKey('auth.User', related_name='reaction', on_delete=models.CASCADE, default=0)
    
# Create your models here.
